## Instructions

Create a Chrome profile store 
```
# Windows 
mkdir c:\chrome_remote
# Linux
mkdir /tmp/chrome_remote
```

Navigate to Chrome executable directory, for example
```
# Windows
cd C:\Program Files (x86)\Google\Chrome\Application
```

Open PowerShell/cmd in the directory and run Chrome with the following command
```
# Windows cmd 
chrome.exe --remote-debugging-port=9222 --user-data-dir=c:\chrome_remote
# Windows PowerShell
.\chrome.exe --remote-debugging-port=9222 --user-data-dir=c:\chrome_remote
```

**Chrome should be opened (see screenshot)**
![pic](./chrome.png)

Done here.




