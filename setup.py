from setuptools import setup, find_packages

setup(
    name='acsi',
    version='0.0.3',
    description='',
    license='MIT',
    packages=find_packages(),
    author='niespodd',
    author_email='d.niespodziany@gmail.com',
    keywords=[],
    url='',
    install_requires=[
        'pyppeteer',
        'requests',
        'urllib3<1.25'
    ]
)
