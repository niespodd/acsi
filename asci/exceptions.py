class ASCICaptchaException(Exception):
    pass


class ASCIConnectionProblem(Exception):
    pass


class ASCIEmptyResponse(Exception):
    pass


class ASCIFetchTimeout(Exception):
    pass
