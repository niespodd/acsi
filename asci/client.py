import asyncio
import logging
from asyncio import sleep
from enum import Enum
from typing import Union, Optional

import requests
from pyppeteer.browser import BrowserContext
from pyppeteer.connection import Connection, CDPSession
from pyppeteer.element_handle import ElementHandle
from pyppeteer.errors import PageError, ElementHandleError
from pyppeteer.target import Target

from asci.exceptions import ASCIFetchTimeout


class SelectorType(Enum):
    XPATH = 1
    QUERY = 2


class Client(object):
    _node_id: str = None
    _url: str = None

    def __init__(self, url: str = None, node_id: str = None):
        self._loop = asyncio.get_event_loop()
        self._node_id = Client._node_id or node_id or "unknown"
        self._url = Client._url or url or "localhost:9222"
        self.__page = asyncio.get_event_loop() \
            .run_until_complete(self._setup_page())

        # `node_id` shall be assigned only in production mode
        if not self._node_id:
            logging.basicConfig(level=logging.DEBUG)

    @staticmethod
    def configure(node_id, url):
        Client._node_id = node_id
        Client._url = url

    async def _setup_page(self):
        browser_json = requests.get("http://{}/json".format(self._url)).json()
        tabs = [page for page in browser_json if page.get("type") == "page"]

        # Clean up anything but first tab
        for tab in tabs[1:]:
            requests.get("http://{}/json/close/{}".format(self._url, tab.get("id")))

        # Setup pyppeteer in a tab scope and mock escalation
        target_info = {**tabs[0], 'targetId': tabs[0]['id']}
        connection = Connection(
            url=target_info["webSocketDebuggerUrl"],
            loop=asyncio.get_event_loop()
        )

        async def _session_factory() -> CDPSession:
            return await connection.createSession(target_info)

        target = Target(
            targetInfo=target_info,
            browserContext=BrowserContext(browser=None, contextId="irrelevant"),  # TODO: Figure out the implications
            ignoreHTTPSErrors=True,
            setDefaultViewport=False,
            screenshotTaskQueue=[],
            loop=asyncio.get_event_loop(),
            sessionFactory=_session_factory
        )
        return await target.page()

    @property
    def _page(self):
        try:
            if not self.__page.url:
                raise Exception
            return self.__page
        except Exception as e:
            logging.error(e)
            self.__page = asyncio.get_event_loop() \
                .run_until_complete(self._setup_page())
            return self.__page


    @staticmethod
    def _await(future):
        return asyncio.get_event_loop().run_until_complete(future)

    def _find_xpath_sel(self, selector: str) -> Union[None, ElementHandle]:
        if "/" in selector:
            try:
                return self._await(self._page.xpath(selector))[0]
            except IndexError:
                pass
        try:
            return self._await(self._page.querySelectorAll(selector))[0]
        except IndexError:
            return None

    def fetch(self, url: str, wait_for: Union[str, int, float] = None, timeout: int = 10) -> str:
        """
        Retrieves HTML content of a website under a specified url.
        :param timeout: (optional) timeout for `wait_for`
        :param wait_for: (optional) selector, javascript property, or timeout to wait for
        :param url: target page url
        :return: page content as a `str`
        """
        self._await(self._page.goto(url))
        if wait_for:
            try:
                self.wait_for(wait_for, timeout=timeout)
            except asyncio.TimeoutError:
                raise ASCIFetchTimeout(url)
        return self._await(self._page.content())

    def click(self, selector: str) -> bool:
        """
        Simulate touch on an HTML element.
        :param selector: element selector or xpath
        :return: True is succeed; otherwise False
        """
        target = self._find_xpath_sel(selector)
        if target:
            self._await(target.tap())
            return True
        else:
            return False

    def type(self, selector: str, value: str) -> bool:
        """
        Simulate typing into an HTML element.
        :param selector: input selector (does not apply to select elements)
        :param value:
        :return: True is succeed; otherwise False
        """
        target = self._find_xpath_sel(selector)
        if target:
            self._await(target.type(value))
            return True
        else:
            return False

    def wait(self, interval: int) -> None:
        """
        Sleeps asynchronously, while letting the browser work.
        :param interval: interval in seconds
        :return:
        """
        return self._await(sleep(interval))

    def get_html(self, selector: str = None) -> Optional[str]:
        """
        :return: html of currently loaded page, or `sel` selector if provided
        """
        if selector:
            match = self._find_xpath_sel(selector)
            if not match:
                logging.warning(f"No node found for {selector}")
                return None
            return self._await(self._page.evaluate("el => el.innerHTML", match))
        return self._await(self._page.content())

    def wait_for(self, selector: str, selector_type: SelectorType = None, timeout: int = 5):
        if not selector_type and "//" in selector:
            selector_type = SelectorType.XPATH

        if not selector_type:
            return self._await(asyncio.wait_for(self._page.waitFor(selector), timeout))
        elif selector_type == SelectorType.XPATH:
            return self._await(asyncio.wait_for(self._page.waitForXPath(selector), timeout))
        elif selector_type == SelectorType.QUERY:
            return self._await(asyncio.wait_for(self._page.waitForSelector(selector), timeout))

    def get_current_url(self):
        return self._page.url

    def element_exists(self, selector: str) -> bool:
        if self._find_xpath_sel(selector):
            return True
        else:
            return False

    def scroll(self, selector: str) -> bool:
        target = self._find_xpath_sel(selector)
        if target:
            self._await(target.hover())
            return True
        else:
            return False
