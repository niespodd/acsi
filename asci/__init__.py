from functools import lru_cache

from .client import Client, SelectorType


_get_client = lru_cache()(lambda: Client())


def lazy_client_func(func_name):
    def wrap_func(*args, **kwargs):
        return getattr(_get_client(), func_name)(*args, **kwargs)
    return wrap_func


fetch = lazy_client_func('fetch')
click = lazy_client_func('click')
type_into = lazy_client_func('type')
get_html = lazy_client_func('get_html')
wait = lazy_client_func('wait')
wait_for = lazy_client_func('wait_for')
get_current_url = lazy_client_func('get_current_url')
element_exists = lazy_client_func('element_exists')
scroll = lazy_client_func('scroll')

SelectorType = SelectorType
